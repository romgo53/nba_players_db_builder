import os
import json
import pathlib
from pymongo import MongoClient
from flask import Flask
from flask import request

MONGODB_HOSTNAME = os.environ['MONGODB_HOSTNAME']
MONGODB_DATABASE = os.environ['MONGODB_DATABASE']


MONGO_URI = 'mongodb://' + MONGODB_HOSTNAME + ':27017/' + MONGODB_DATABASE


myclient = MongoClient(MONGODB_HOSTNAME, 27017)
my_db = myclient[MONGODB_DATABASE]


def testing():
    print("All is OK!!!")


def create_collections():
    directory = 'DB_Creator/'
    file_name = 'filePlayers.json'

    with open(file_name) as f:
        file_data = json.load(f)
        file_name_no_postfix = file_name.split(".")[0]
        collection = my_db[file_name_no_postfix]
        collection.insert_one(file_data)


def check_db(db_name):
    dblist = myclient.list_database_names()
    if db_name in dblist:
        print("The database exists.")


def check_collection(collection_name):
    collist = my_db.list_collection_names()
    if collection_name in collist:
        print("The collection exists.")


def get_all_items_from_collection(collection_name):
    db_list = []
    col = my_db[collection_name]
    for entry in col.find():
        db_list.append(entry)

    return db_list


def close():
    myclient.close()


create_collections()